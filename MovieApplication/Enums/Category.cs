﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MovieApplication.Enums
{
    public enum Category
    {
        Action,
        Comedy,
        Horror,
        Romance,
        Fiction
    }
}