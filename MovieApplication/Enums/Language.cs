﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MovieApplication.Enums
{
    public enum Language
    {
        English,
        Italian,
        Spanish,
        Cantonese,
        Taiwanese
    }
}