﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using MovieApplication.Enums;

namespace MovieApplication.Models
{

    public class Movie
    {
        [Key]
        public int MovieID { set; get; }

        [Required(ErrorMessage = "Please Enter The Movie Name")]
        public string Name { set; get; }

        public string Cover { set; get; }

        [Required(ErrorMessage = "Please Enter The Movie Synopsis")]
        public string Synopsis { get; set; }

        [Required(ErrorMessage = "Please Enter The Trail URL")]
        public string Trailer { get; set; }

        public Category Category { get; set; }

        public Language Language { get; set; }

        public double AverageRating
        {
            private set { }
            get
            {
                if (this.Reviews == null)
                    return 0;
                else if (this.Reviews.Where(r => r.Rating > 0).Count() == 0)
                    return 0;
                return this.Reviews.Where(r => r.Rating > 0).Average(r => r.Rating);
            }

        }

        [Required(ErrorMessage = "Please Enter The Movie Length")]
        [Range(1,300 , ErrorMessage ="Please Enter A Number Between 1 And 300")]
        public int Length { get; set; }


        public virtual ICollection<Review> Reviews { set; get; }

        public virtual ICollection<Session> Sessions { get; set; }

         

    }
}