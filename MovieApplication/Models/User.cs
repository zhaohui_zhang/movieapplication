﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace MovieApplication.Models
{
    public class User : IdentityUser
    {
        private string lastName, firstName;

        public  ICollection<Transaction> Transactions { get; set; }

        public string LastName
        {
            set
            {
                if (value != null)
                    lastName = CapitaliseFirstLetter(value);
                else
                    lastName = null;
            }
            get
            {
                return lastName;
            }
        }


        public string FirstName
        {
            set
            {
                if (value != null)
                    firstName = CapitaliseFirstLetter(value);
                else
                    firstName = null;
            }
            get
            {

                return firstName;
            }
        }

 
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<User> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        private string CapitaliseFirstLetter(string input)
        {

            return input.Substring(0, 1).ToUpper() + input.Substring(1);
        }
    }
}