﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MovieApplication.Models
{
    public class Session
    {
        [Key]
        public int SessionID { get; set; }

        
        public int TicketPrice { get; set; }

        
        public DateTime Time { get; set; }

        [ForeignKey("Cinema")]
        public int CinemaID { get; set; }

        public Cinema Cinema { get; set; }

        [ForeignKey("Movie")]
        public int MovieID { get; set; }

        public Movie Movie { get; set; }

        public virtual ICollection<Seat> Seats { get; set; }

        public  ICollection<Transaction> Transactions { get; set; }
    }
}