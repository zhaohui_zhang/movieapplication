﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MovieApplication.Models
{
    public class Review
    {
        [Key]
        public int ReviewID { get; set; }

        [Required(ErrorMessage ="Please Enter Your Review")]
        public string Comment { get; set; }

        [Required(ErrorMessage = "Please Rate The Movie")]
        public int Rating { get; set; }

        [ForeignKey("Movie")]
        public int MovieID { get; set; }

        public Movie Movie { get; set; }

    }
}