﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace MovieApplication.Models
{
    public class Ticket
    {
        [Key]
        public int TicketID { set; get; }

        public int RowNumber { set; get; }

        public int SeatNumber { set; get; }
    }
}