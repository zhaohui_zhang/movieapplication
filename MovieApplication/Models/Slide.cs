﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MovieApplication.Models
{
    public class Slide
    {
        public int SlideID { get; set; }

        public int Order { get; set; }

        public string Path { get; set; }
    }
}