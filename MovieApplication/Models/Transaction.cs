﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MovieApplication.Models
{
    public class Transaction
    {
        [Key]
        public int TransactionID { get; set; }

        public int Amount { get; set; }

        public DateTime Date { get; set; }

        [ForeignKey("Session")]
        public int SessionID { get; set; }

        public Session Session { get; set; }

        [ForeignKey("User")]
        public string UserID { get; set; }

        public User User { get; set; }
    }
}