﻿using MovieApplication.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MovieApplication.Models
{
    public class Seat
    {
        [Key, Column(Order = 1)]
        public int SeatNumber { get; set; }
       
        [ForeignKey("Session")]
        [Key , Column(Order = 2)]
        public int SessionID { get; set; }

        public Session Session { get; set; }

        public SeatAvailability Availability { get; set; }
    }
}