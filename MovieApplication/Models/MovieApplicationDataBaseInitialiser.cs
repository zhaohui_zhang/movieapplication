﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using MovieApplication.Enums;

namespace MovieApplication.Models
{
    public class MovieApplicationDataBaseInitialiser : DropCreateDatabaseIfModelChanges<MovieApplicationContext>
    {
        
        protected override void Seed(MovieApplicationContext context)
        {
            var Cinemas = new List<Cinema>() {

                new Cinema { Row = 5 , Column = 15 }  ,
                new Cinema { Row = 6 , Column = 6 }  ,
 
            };

            var Movies = new List<Movie>()
            {
                new Movie { Name = "Maze Runner 2" , Cover = "/Content/Covers/mazerunner2.jpg" ,Synopsis ="A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart. I am alone, and feel the charm of existence in this spot, which was created for the bliss of souls like mine. I am so happy, my dear friend, so absorbed in the exquisite sense of mere tranquil existence, that I neglect my talents. I should be incapable of drawing a single stroke at the present moment; and yet I feel that I never was a greater artist than now. When, while the lovely valley teems with vapour around me, and the meridian sun strikes the upper surface of the impenetrable foliage of my trees, and but a few stray gleams steal into the inner sanctuary, I throw myself down among the tall grass by the trickling stream; and, as I lie close to the earth, a thousand unknown plants are noticed by me: when I hear the buzz of the little world among the stalks, and grow familiar with the countless indescribable forms of the insects and flies, then I feel the presence of the Almighty, who formed us in his own image, and the breath" , Trailer="https://www.youtube.com/embed/K1AACTA7OtA" , Category= Category.Fiction , Length= 120 , Language= Language.English
                , Sessions = new List<Session> ()
                {
                    new Session () {  Time = DateTime.Parse("2016-04-14 09:30 ") , TicketPrice = 10 , CinemaID=1 , } ,
                    new Session () {  Time = DateTime.Parse("2016-07-15 16:30 ") , TicketPrice = 12, CinemaID =1 } ,
                     new Session () {  Time = DateTime.Parse("2016-03-17 13:30 ") , TicketPrice = 10 , CinemaID=1} ,
                    new Session () {  Time = DateTime.Parse("2016-03-17 10:30 ") , TicketPrice = 12, CinemaID =1 } ,
                }
                },
                new Movie { Name = "Fast & Furious 7" , Cover = "/Content/Covers/fastandfurious7.jpg" ,  Synopsis ="A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart. I am alone, and feel the charm of existence in this spot, which was created for the bliss of souls like mine. I am so happy, my dear friend, so absorbed in the exquisite sense of mere tranquil existence, that I neglect my talents. I should be incapable of drawing a single stroke at the present moment; and yet I feel that I never was a greater artist than now. When, while the lovely valley teems with vapour around me, and the meridian sun strikes the upper surface of the impenetrable foliage of my trees, and but a few stray gleams steal into the inner sanctuary, I throw myself down among the tall grass by the trickling stream; and, as I lie close to the earth, a thousand unknown plants are noticed by me: when I hear the buzz of the little world among the stalks, and grow familiar with the countless indescribable forms of the insects and flies, then I feel the presence of the Almighty, who formed us in his own image, and the breath" , Category= Category.Action , Length= 150 , Language = Language.English , Trailer="https://www.youtube.com/embed/K1AACTA7OtA"

                } ,
                                new Movie { Name = "Inception" , Cover = "/Content/Covers/inception.jpg" ,  Synopsis ="A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart. I am alone, and feel the charm of existence in this spot, which was created for the bliss of souls like mine. I am so happy, my dear friend, so absorbed in the exquisite sense of mere tranquil existence, that I neglect my talents. I should be incapable of drawing a single stroke at the present moment; and yet I feel that I never was a greater artist than now. When, while the lovely valley teems with vapour around me, and the meridian sun strikes the upper surface of the impenetrable foliage of my trees, and but a few stray gleams steal into the inner sanctuary, I throw myself down among the tall grass by the trickling stream; and, as I lie close to the earth, a thousand unknown plants are noticed by me: when I hear the buzz of the little world among the stalks, and grow familiar with the countless indescribable forms of the insects and flies, then I feel the presence of the Almighty, who formed us in his own image, and the breath" , Trailer="https://www.youtube.com/embed/K1AACTA7OtA" , Length= 150 , Category = Category.Fiction , Language = Language.English } ,


            };



            Cinemas.ForEach(c => context.Cinemas.Add(c));
            Movies.ForEach(m => context.Movies.Add(m));
            context.SaveChanges();

            var sessions = context.Sessions.Where(s => s.CinemaID == 1).ToList();
            var cinema = context.Cinemas.Find(1);
            sessions.ForEach(s =>
            {
                s.Seats = new List<Seat>();
                for (var i = 1; i <= cinema.Capacity; i++)
                {
                    s.Seats.Add(new Seat() { Availability = SeatAvailability.FREE, SeatNumber = i, SessionID = s.SessionID });
                }
            });


            var slides = new List<Slide>() {
                new Slide () { Order=1 , Path = "/Content/Slides/deadpool.jpg" } ,
                new Slide () { Order=2 , Path = "/Content/Slides/hailcaesar.jpg" } ,
                new Slide () { Order=3 , Path = "/Content/Slides/kungfupanda.jpg" } ,
            };

            slides.ForEach(s=> context.Slides.Add(s));

            var roles = new List<Role>() {
                new Role("Admin") ,
                new Role("User"),
            };

            roles.ForEach(r => context.Roles.Add(r));
            context.SaveChanges();
        }
        
    }
}