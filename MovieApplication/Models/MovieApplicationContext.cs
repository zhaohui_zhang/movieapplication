﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;

namespace MovieApplication.Models
{
    public class MovieApplicationContext : IdentityDbContext<User>
    {
        public MovieApplicationContext() : base("MovieApplicationContext") { }
        public DbSet<Movie> Movies { set; get; }
        public DbSet<Cinema> Cinemas { set; get; }
        public DbSet<Session> Sessions { get; set; }
        public DbSet<Review> Reviews { get; set; }
        public DbSet<Seat> Seats { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<Slide> Slides { get; set; }
        protected override void OnModelCreating(System.Data.Entity.DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<IdentityUser>().ToTable("Users").Property(p => p.Id).HasColumnName("UserID");
            modelBuilder.Entity<User>().ToTable("Users").Property(p => p.Id).HasColumnName("UserID");
            modelBuilder.Entity<IdentityRole>().ToTable("Roles").Property(p => p.Id).HasColumnName("RoleID");
            modelBuilder.Entity<Role>().ToTable("Roles").Property(p => p.Id).HasColumnName("RoleID");
        }


        public static MovieApplicationContext Create()

        {

            return new MovieApplicationContext();

        }

    }



}