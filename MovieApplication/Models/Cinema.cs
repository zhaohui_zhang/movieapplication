﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using MovieApplication.Enums;

namespace MovieApplication.Models
{
    public class Cinema
    {
 
        [Key]
        public int CinemaID { get; set; }

        [Range(1, int.MaxValue , ErrorMessage = "Please Enter A Valid Row Number")]
        public int Row {    set; get; }

        [Range(1,20 , ErrorMessage = "Enter A Number Between 1 and 20")]
        public int Column {set; get;}

        public int Capacity {

            private set { } 

            get {
                return Column * Row;
            }
        }

        public virtual ICollection<Session> Sessions { get; set; }

 
    }
}