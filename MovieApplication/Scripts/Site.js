﻿
$(document).ready(function () {

    //  to add little animation to the footer section 
    $("footer .col").hover(function () {
        $(this).find(".arrow-down").css("border-top-color", $(this).css("border-top-color"));
        $(this).find(".arrow-down").css("visibility", "visible");
    }, function () {
        $(this).find(".arrow-down").css("visibility", "hidden");
    });


    $(".loading_btn").on("click", function () {
        LoaderOn();
    })


    // now showing button
    $("button[name=nowShowingButton]").click(function () {

        $("#myMovieWindow_content > div:nth-child(1)").show();
        $("#myMovieWindow_content > div:nth-child(2)").hide();
    });

    // coming soon button
    $("button[name=comingSoonButton]").click(function () {

        $("#myMovieWindow_content > div:nth-child(1)").hide();
        $("#myMovieWindow_content > div:nth-child(2)").show();
    });

    var clickedValue = 0;
    $(".radio_group label").hover(function () {
        $(this).siblings().css("color", "black");
        $(this).prevAll("label").css("color", "gold");
        $(this).css("color", "gold");


    }, function () {
        if (clickedValue == 0) {
            $(this).siblings().css("color", "black");
            $(this).css("color", "black");
        } else {
            var clickedStar = $(".radio_group input[value=" + clickedValue + "]").parent();
            clickedStar.siblings().css("color", "black");
            clickedStar.prevAll("label").css("color", "gold");
            clickedStar.css("color", "gold");
        }
    });

    $(".radio_group label").click(function () {
        $(this).siblings().css("color", "black");
        $(this).prevAll("label").css("color", "gold");
        $(this).css("color", "gold");
        clickedValue = $(this).find("input").val();
    });


    // to validate the comment 
    $("#myReviewPanel textarea").blur(function () {
        ValidateReview();
    });


    // to validate the rating whenever the rating star is pressed 
    $("#myReviewPanel input[type=radio]").change(function () {
        ValidateReview();
    });



    // post button in ViewAMovie.cshtml
    $("#myReviewPanel button[name=postReview]").click(function () {
        var panel = $("#myReviewPanel");
        var review = { "Comment": panel.find("textarea").val(), "Rating": panel.find("input:checked").val() };
        LoaderOn();
        $.ajax({
            url: $(this).attr("data-url"),
            type: "POST",
            data: JSON.stringify(review),
            contentType: "application/json; charset=utf-8",
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert(errorThrown);
            },
            success: function (result) {
                $("#myReviewsBox").html(result);
            },
            complete: function () {
                LoaderOff();
            }
        });
    });


    $("button.changeDate").click(function () {
        $("input.changeDate").toggle();
    });

    // when the selected date is changed
    $("input.changeDate").change(function () {
        var date = { "Date": $(this).val() };
        $.ajax({
            url: $("button.changeDate").attr("data-url"),
            type: "GET",
            contentType: "application/json; charset=utf-8",
            data: date,
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert(errorThrown);
            },
            success: function (result) {
                $("#mySessionsWindow").html(result);
                $("input.changeDate").val(date["Date"]);
                $("span.changeDate").text(date["Date"]);
                SelectSeat();
                TicketQuantitySelect();
                $("#paymentSummary").empty();
                $("#seatSummary").empty();
            }
        });
        $("input.changeDate").toggle();
    });

    SelectSeat();


    // enable the button only if one of the session is chosen 

    TicketQuantitySelect();
    $("button[name=payButton]").click(function () {
        LoaderOn();
        var cardInfo = {
            "CardNumber": $("#paymentInformation input[name=CardNumber]").val(),
            "HolderName": $("#paymentInformation input[name=HolderName]").val(),
            "ExpiryMonth": $("#paymentInformation select[name=ExpiryMonth]").val(),
            "ExpiryYear": $("#paymentInformation select[name=ExpiryYear]").val(),
            "CCV": $("#paymentInformation input[name=CCV]").val(),
            "SelectedSeats": selectedSeat,
            "SessionID": selectedSessionID,
            "Total": total,
        };
        CreateStripeToken(cardInfo);
    });

    // upload slides on dashboard
    $("#browseButton input[type=file]").change(function () {
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.readAsDataURL(this.files[0]);
            var fd = new FormData();
            fd.append("Upload", this.files[0]);
            LoaderOn();
            $.ajax({
                url: "/Admin/AddSlide",
                type: "POST",
                data: fd,
                processData: false,
                contentType: false,
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    alert(errorThrown);
                },
                success: function (result) {
                    if (result > 0) {
                        $("#indexSlides .wrapper").append("<div class='frame'><p class='delete_symbol' data_url='/Admin/DeleteSlide/" + result + "'>X</p><img src='" + reader.result + "' /></div>");
                        DeleteSlide();
                    }
                    else {
                        ShowWarningModal(result);

                    }
                },
                complete: function () {
                    LoaderOff();
                },

            });
        }
    });

    // delete slide on dashboard
    DeleteSlide();


    // create side navigation bar for about pages 
    $(window).load(  function () {
        CreateSideNav();
       
    });;

    // make animation for the book now button 
    $("#myProBroad input[type=button]").click(function () {
        $("body").animate({ scrollTop: $("#myMovieWindow").offset().top }, 1000);
      });


    // self-invoked function , shorten synopsis unless the user move mouse over it 
    (function () {
        if ($("#myViewMovie #movieSynopsis p").height() > $("#myViewMovie #movieSynopsis .wrapper").height()) {
            $("#myViewMovie #movieSynopsis ").hover(function () {
                $("#myViewMovie #movieSynopsis .wrapper ").css("overflow", "auto");
            }, function () {
                $("#myViewMovie #movieSynopsis .wrapper ").css("overflow", "hidden");
            });
        }
    })();

    
 

});


function CreateSideNav() {
    var titles = $("#aboutLayout .main .section .section_title");
    var nav = "<ul>";
    var offsetTops = [];
    for (var i = 0 ; i < titles.length ; i++) {
        offsetTops[titles[i].getAttribute("id")] = Math.floor( $("#aboutLayout .main .section:nth-child(" + (i + 1) + ")> .section_title ").offset().top );
        nav += "<li><a data_parent='" + titles[i].getAttribute("id") + "' href='#" + titles[i].getAttribute("id") + "'>" + titles[i].innerText + "</a></li>";
    }
    nav += "</ul>";
    var wrapper = $("#aboutLayout .complementary .wrapper");
    wrapper.append(nav);
    wrapper.find("ul > li:nth-child(1)  ").css("background-color", "gold");
    wrapper.find("a").click(function () {
        $(this).parent().siblings().css("background-color", "initial");
        $(this).parent().css("background-color", "gold");
    });

    $(window).scroll(function () {
        var entry = "";
        var preEntry = "";
        for (var key in offsetTops) {
            preEntry = entry;
            entry = key;
            if (window.pageYOffset < offsetTops[key]) {
                break;
            }
        }
        if (window.pageYOffset >= offsetTops[entry])
            preEntry = entry;
        $("#aboutLayout .complementary  a[data_parent=" + preEntry + "]").parent().siblings().css("background-color", "initial");
        $("#aboutLayout .complementary  a[data_parent=" + preEntry + "]").parent().css("background-color", "gold");
    });

}

function DeleteSlide() {
    $("#indexSlides .frame .delete_symbol").click(function () {
        var slide = $(this);
        LoaderOn();
        $.ajax({
            url: slide.attr("data_url"),
            type: "GET",
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert(errorThrown);

            },
            success: function (result) {
                if (result == "")
                    slide.parent().remove();
            },
            complete: function () {
                LoaderOff();
            }
        });
    });
}

function ShowWarningModal(content) {
    $('#myModal').remove();
    $("body").append(content);
    $('#myModal').modal('show');

}

function CreateStripeToken(cardInfo) {
    Stripe.setPublishableKey("pk_test_F2708XjaASEcYDA1rzkchWIj"); // test
    Stripe.card.createToken({
        number: cardInfo.CardNumber,
        cvc: cardInfo.CCV,
        exp_month: cardInfo.ExpiryMonth,
        exp_year: cardInfo.ExpiryYear,
    }, function (status, response) {
        cardInfo.Token = response.id;
        if (response.error)
            cardInfo.ErrorMessage = response.error.message;
        else
            cardInfo.ErrorMessage = "";
        $.ajax({
            url: "/Booking/AcceptPayment",
            type: "POST",
            data: JSON.stringify(cardInfo),
            contentType: "application/json; charset=utf-8",
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert(errorThrown);
            },
            success: function (result) {
                if (result == "") {
                    window.location.replace("/Booking/BookingSucceed");
                }
                else {
                    ShowWarningModal(result);
                    LoaderOff();
                }
            }

        });

    });
}



function LoaderOn() {
    $("body").append("<div class='loading'><div class='loader'></div></div>");
}

function LoaderOff() {
    $(".loading").remove();

}

// to see if the user has enterred a valid comment and rating 
function ValidateReview() {
    var panel = $("#myReviewPanel");
    if (panel.find("textarea").val() != "" && panel.find("input:checked").val() != undefined) {
        $("#myReviewPanel button").prop("disabled", false);
    }
    else {
        $("#myReviewPanel button").prop("disabled", true);
    }
}



var ticketQuantity = 0;
var selectedSessionID = 0;
var total = 0;
function TicketQuantitySelect() {

    $("#sessionSelection table select").change(function () {
        var selects = $("#sessionSelection table").find("select");
        var selectedNum = 0;
        var index = 0;
        for (var i = 0 ; i < selects.length ; i++) {
            if (selects[i].value != 0) {
                selectedNum++;
                index = i;
            }
        }
        $("#paymentSummary ").empty();
        $("#seatSummary").empty();
        if (selectedNum != 1) {
            $("button[name='selectSeat']").prop("disabled", true);

        }
        else {
            $("button[name='selectSeat']").prop("disabled", false);
            $("button[name='selectSeat']").attr("data-url", "/Booking/RetrieveSeats/" + selects[index].getAttribute("sessionID"));
            selectedSessionID = selects[index].getAttribute("sessionID");
            var ticketPrice = selects[index].getAttribute("ticketPrice");
            ticketQuantity = selects[index].value;
            total = ticketQuantity * ticketPrice;
            $("#paymentSummary ").append("<hr/><h4>Total:</h4><p>Ticket X " + ticketQuantity + " :<span></span><span class='float_right'>$" + total + "</span></p>");
        }

    });
}



var selectedSeat = [];
function SelectSeat() {
    $("button[name='selectSeat']").click(function () {
        LoaderOn();
        selectedSeat = []; // reset the array whenever the button is clicked
        $("button[name=payButton]").prop("disabled", true);
        $("#seatSummary").empty();
        $.ajax(
            {
                url: $("button[name='selectSeat']").attr("data-url"),
                type: "GET",
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    alert(errorThrown);
                },
                success: function (result) {
                    $(".loading").remove();
                    $("body").append("<div id='cinemaLayout'>" + result + "</div>");
                    $("#cinemaLayout button[name=seatCancelButton]").click(function () {
                        $("#cinemaLayout").remove();
                    });
                    $("#cinemaLayout button[name=seatConfirmButton]").click(function () {
                        // $("#seatSummary").empty();
                        $("#cinemaLayout").remove();
                        var output = "";
                        for (var i = 0 ; i < selectedSeat.length ; i++) {
                            output += "<li>No." + selectedSeat[i] + "</li>";
                        }
                        $("#seatSummary").append("<hr/><h4>Selected Seats:</h4><ul>" + output + "</ul>");
                        $("button[name=payButton]").prop("disabled", false);

                    });
                    $(".seatAva_FREE").click(function () {

                        if ($(this).css("background-color") == "rgb(205, 133, 63)") // peru 
                        {
                            $(this).css("background-color", "seagreen");
                            selectedSeat.pop();
                        }
                        else if ($(this).css("background-color") == "rgb(46, 139, 87)") // seagreen
                        {
                            $(this).css("background-color", "peru");
                            selectedSeat.push($(this).attr("seatNumber"));
                        }

                        if (selectedSeat.length == ticketQuantity)
                            $("#cinemaLayout button[name=seatConfirmButton]").prop("disabled", false);
                        else
                            $("#cinemaLayout button[name=seatConfirmButton]").prop("disabled", true);
                    });

                }
            });


    });
}


