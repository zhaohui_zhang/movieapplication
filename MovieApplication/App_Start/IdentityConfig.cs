﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using MovieApplication.Models;

namespace MovieApplication
{


    // Configure the application user manager used in this application. UserManager is defined in ASP.NET Identity and is used by the application.
    public class MovieApplicationUserManager : UserManager<User>
    {
        public MovieApplicationUserManager(IUserStore<User> store) : base(store)
        {
        }

        public static MovieApplicationUserManager Create(IdentityFactoryOptions<MovieApplicationUserManager> options, IOwinContext context)
        {
            var manager = new MovieApplicationUserManager(new UserStore<User>(context.Get<MovieApplicationContext>()));

            manager.UserValidator = new UserValidator<User>(manager)
            {
                RequireUniqueEmail = true,
                AllowOnlyAlphanumericUserNames = false,

            };
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,

            };


            return manager;
        }
    }

    public class MovieApplicationSignInManager : SignInManager<User, string>
    {
        public MovieApplicationSignInManager(MovieApplicationUserManager userManager, IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        {
        }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(User user)
        {
            return user.GenerateUserIdentityAsync((MovieApplicationUserManager)UserManager);
        }

        public static MovieApplicationSignInManager Create(IdentityFactoryOptions<MovieApplicationSignInManager> options, IOwinContext context)
        {
            return new MovieApplicationSignInManager(context.GetUserManager<MovieApplicationUserManager>(), context.Authentication);
        }
    }


    public class MovieApplicationRoleManager : RoleManager<Role>
    {
        public MovieApplicationRoleManager( IRoleStore<Role, string> roleStore)
        : base(roleStore)
        {
        }
        public static MovieApplicationRoleManager Create(
            IdentityFactoryOptions<MovieApplicationRoleManager> options, IOwinContext context)
        {
            return new MovieApplicationRoleManager(
                new RoleStore<Role>(context.Get<MovieApplicationContext>()));
        }

    }
}
