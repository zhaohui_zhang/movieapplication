﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MovieApplication.Models;
using MovieApplication.ViewModels;
using System.IO;
using System.Data.Entity;
using MovieApplication.Enums;

namespace MovieApplication.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        private MovieApplicationContext database;

        public AdminController() : base()
        {

            this.database = new MovieApplicationContext();

        }

        public ActionResult Index()
        {
            var moviesByRevenue = this.database.Movies.Include(m => m.Sessions.Select(s => s.Transactions)).OrderByDescending(m => m.Sessions.Sum(s => s.Transactions.Sum(t => t.Amount))).ToList();
            var moviesByRating = moviesByRevenue.OrderByDescending(m => m.AverageRating).ToList();
            var sessions = this.database.Sessions.Include(s => s.Seats).OrderByDescending(s => s.Seats.Count(seat => seat.Availability == SeatAvailability.TAKEN)).ToList();
            var slides = this.database.Slides.ToList();
            return View("DashBoard", new DashBoardViewModel() { MoviesByRating = moviesByRating, MovieByRevenue = moviesByRevenue, Sessions = sessions, Slides = slides });
        }

        [HttpPost]
        public ActionResult AddSlide(HttpPostedFileBase Upload)
        {
            if (Upload != null && Upload.ContentType.Contains("image"))
            {
                var virtualPath = "/Content/Slides/";
                Upload.SaveAs(Path.Combine(Server.MapPath(virtualPath), Path.GetFileName(Upload.FileName)));
                var slide = new Slide() { Order = this.database.Slides.Count() + 1, Path = virtualPath + Path.GetFileName(Upload.FileName) };
                this.database.Slides.Add(slide);
                this.database.SaveChanges();
                return Content(slide.SlideID.ToString());
            }
            else
                return PartialView("WarningModal", ViewBag.Content = "Please Select An Image File");

        }


        public ActionResult DeleteSlide(int id)
        {
            var slide = this.database.Slides.Find(id);
            var path = Server.MapPath(slide.Path);
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
            }
            this.database.Slides.Remove(slide);
            this.database.SaveChanges();
            return new EmptyResult();
        }

        public ActionResult ManageMovies()
        {
            var movies = this.database.Movies.OrderByDescending(m => m.MovieID).ToList();
            return View("ManageMovie", new ManageMovieViewModel() { Movies = movies, AddedMovie = new Movie() });

        }

        [HttpPost]
        public ActionResult AddMovie(Movie movie, HttpPostedFileBase Upload)
        {

            if (Upload == null || Upload.ContentLength <= 0)
            {
                ModelState.AddModelError("", "You Did Not Upload Any File");
            }
            else if (!Upload.ContentType.Contains("image"))
            {
                ModelState.AddModelError("", "Please Upload An Image File");
            }
            if (ModelState.IsValid)
            {
                var path = Path.Combine(Server.MapPath("/Content/Covers/"), Path.GetFileName(Upload.FileName));
                Upload.SaveAs(path);
                movie.Cover = "/Content/Covers/" + Path.GetFileName(Upload.FileName);
                this.database.Movies.Add(movie);
                this.database.SaveChanges();
            }
            else {
                var vm = new ManageMovieViewModel() { AddedMovie = movie, Movies = this.database.Movies.ToList() };
                return View("ManageMovie", vm);
            }


            return RedirectToAction("ManageMovies");

        }


        public ActionResult DeleteMovie(int id)
        {
            var movie = this.database.Movies.Single(m => m.MovieID == id);
            if (System.IO.File.Exists(movie.Cover))
            {
                System.IO.File.Delete(movie.Cover);
            }
            this.database.Movies.Remove(movie);
            this.database.SaveChanges();
            return RedirectToAction("ManageMovies");

        }



        [HttpPost]
        public ActionResult EditMovie(int id, Movie movie, HttpPostedFileBase Upload)
        {
            if (ModelState.IsValid)
            {
                var existingMovie = this.database.Movies.Single(m => m.MovieID == id);
                if (Upload != null && Upload.ContentLength > 0 && Upload.ContentType.Contains("image"))
                {
                    string oldFilePath = Path.Combine(Server.MapPath("/Content/Covers"), Path.GetFileName(existingMovie.Cover));
                    if (System.IO.File.Exists(oldFilePath))
                        System.IO.File.Delete(oldFilePath);
                    existingMovie.Cover = "/Content/Covers/" + Path.GetFileName(Upload.FileName);
                    Upload.SaveAs(Path.Combine(Server.MapPath("/Content/Covers"), Path.GetFileName(Upload.FileName)));

                }
                existingMovie.Category = movie.Category;
                existingMovie.Language = movie.Language;
                existingMovie.Length = movie.Length;
                existingMovie.Name = movie.Name;
                existingMovie.Synopsis = movie.Synopsis;
                existingMovie.Trailer = movie.Trailer;
                this.database.SaveChanges();
                return RedirectToAction("ManageMovies");
            }
            else {


                return View("ManageMovie", new ManageMovieViewModel() { AddedMovie = movie, Movies = this.database.Movies.ToList() });
            }


        }


        public ActionResult ManageSessions()
        {
            var movies = this.database.Movies.ToList();
            var cinemas = this.database.Cinemas.ToList();
            return View("ManageSession", new ManageSessionViewModel() { Movies = movies, Cinemas = cinemas });
        }


        [HttpPost]
        public ActionResult AddSession(ManageSessionViewModel vm)
        {


            if (ModelState.IsValid)
            {
                var session = new Session();
                session.CinemaID = vm.CinemaID;
                session.MovieID = vm.MovieID;
                session.TicketPrice = vm.Price;
                session.Time = DateTime.Parse(vm.Date);
                session.Time = session.Time.AddHours(vm.Hour);
                session.Time = session.Time.AddMinutes(vm.Minute);
                var cinema = this.database.Cinemas.Find(vm.CinemaID);
                session.Seats = new List<Seat>();
                for (var i = 1; i <= cinema.Capacity; i++)
                {
                    session.Seats.Add(new Seat() { Availability = SeatAvailability.FREE, SeatNumber = i, SessionID = session.SessionID });
                }
                this.database.Sessions.Add(session);
                this.database.SaveChanges();
                return RedirectToAction("ManageSessions");
            }
            else {

                vm.Movies = this.database.Movies.ToList();
                vm.Cinemas = this.database.Cinemas.ToList();
                return View("ManageSession", vm);
            }


        }


        public ActionResult ManageCinemas()
        {

            var cinemas = this.database.Cinemas.ToList();
            return View("ManageCinema", new ManageCinemaViewModel() { Cinemas = cinemas });
        }


        public ActionResult AddCinema(Cinema cinema)
        {

            if (ModelState.IsValid)
            {
                this.database.Cinemas.Add(cinema);
                this.database.SaveChanges();
                return RedirectToAction("ManageCinemas");
            }
            else {

                return ManageCinemas();

            }

        }

        public ActionResult DeleteCinema(int id)
        {

            var cinema = this.database.Cinemas.Find(id);
            this.database.Cinemas.Remove(cinema);
            this.database.SaveChanges();
            return RedirectToAction("ManageCinemas");

        }

        public ActionResult DeleteSession(int id)
        {
            var session = this.database.Sessions.Find(id);
            this.database.Sessions.Remove(session);
            this.database.SaveChanges();
            return RedirectToAction("ManageSessions");
        }
    }
}