﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MovieApplication.ViewModels;
using MovieApplication.Models;
namespace MovieApplication.Controllers
{
    public class MovieController : Controller
    {

        private MovieApplicationContext dataBase;


        public MovieController() : base()
        {
            this.dataBase = new MovieApplicationContext();

        }

        public ActionResult ViewAMovie(int id)
        {
            var movie = this.dataBase.Movies.Single(m => m.MovieID == id);
            return View("ViewAMovie", new ViewAMovieViewModel() { Movie = movie, });
        }

        public ActionResult RetrieveReviews(int id)
        {
            var movie = this.dataBase.Movies.Single(m => m.MovieID == id);
            var reviews = movie.Reviews.OrderByDescending(r => r.ReviewID).ToList();
            return PartialView("MovieReviewsPartial", new ReviewsViewModel() { Reviews = reviews });

        }

        [HttpPost]
        public ActionResult PostAReview(int id, Review review)
        {
            review.MovieID = id;
            this.dataBase.Reviews.Add(review);
            this.dataBase.SaveChanges();
            return RedirectToAction("RetrieveReviews", new { id = id });
        }



    }
}