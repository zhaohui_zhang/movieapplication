﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MovieApplication.Models;
using MovieApplication.ViewModels;
using System.Collections;
using MovieApplication.Enums;

namespace MovieApplication.Controllers
{
    public class HomeController : Controller
    {
        private MovieApplicationContext dataBase;

        public HomeController()
        {

            this.dataBase = new MovieApplicationContext();
        }


        // GET: Home
        public ActionResult Index()
        {
            return View(new IndexViewModel() { Slides = this.dataBase.Slides.ToList() });
        }

        public ActionResult AboutApplication()
        {
            return View();
        }

        public ActionResult AboutDeveloper()
        {
            return View();
        }

        public ActionResult SignUp()
        {
            GetReturnUrl();
            return View("SignUp", new SignUpViewModel());
        }

        public ActionResult SignIn()
        {
            GetReturnUrl();
            return View("SignIn", new SignInViewModel());
        }

        //[ChildActionOnly]
        public ActionResult MovieGallary(MovieType type)
        {
            return PartialView("MovieGallaryPartial", new MoviesViewModel()
            {
                Movies = ReturnMoviesByType(type)
            });
        }

        private void GetReturnUrl()
        {
            try
            {
                var returnUrl = Request.UrlReferrer.AbsolutePath;
                if (Url.IsLocalUrl(returnUrl))
                    TempData["returnURL"] = returnUrl;
            }
            catch (Exception e)
            {
                TempData["returnURL"] = null;
            }

        }


        private ICollection<Movie> ReturnMoviesByType(MovieType type)
        {
            var movies = new List<Movie>();
            var tomorrow = DateTime.Today.Date.AddDays(1);
             switch (type)
            {
                case MovieType.NowShowing:
                    movies = this.dataBase.Movies.Where(m => m.Sessions.Min(s => s.Time) < tomorrow && m.Sessions.Max(s => s.Time) >= DateTime.Today).ToList();
                    break;
                case MovieType.ComingSoon:
                    movies = this.dataBase.Movies.Where(m => m.Sessions.Min(s => s.Time) >= tomorrow).ToList();
                    break;
            }
            return movies;
        }


 
    }
}