﻿using MovieApplication.Enums;
using MovieApplication.Models;
using MovieApplication.ViewModels;
using System;
using System.Linq;
using System.Web.Mvc;
using iTextSharp.text;
using System.IO;
using iTextSharp.text.pdf;
using System.Threading.Tasks;
using Stripe;
using Microsoft.AspNet.Identity;
using System.Data.Entity;

namespace MovieApplication.Controllers
{
    public class BookingController : Controller
    {
        private MovieApplicationContext dataBase;

        public BookingController() : base()
        {
            this.dataBase = new MovieApplicationContext();
        }

        public ActionResult BookAMovie(int id, BookAMovieViewModel vm = null)
        {
            var movie = this.dataBase.Movies.SingleOrDefault(m => m.MovieID == id);
            return View("BookAMovie", new BookAMovieViewModel() { Movie = movie });
        }

        public ActionResult RetrieveMovieSessions(int id, DateTime date)
        {
            var movie = this.dataBase.Movies.SingleOrDefault(m => m.MovieID == id);
            var sessions = movie.Sessions.Where(s => s.Time.Date == date.Date).ToList();
            if (sessions.Count == 0)
            {
                var afterSessions = movie.Sessions.Where(s => s.Time.Date > date.Date);
                var dates = string.Empty;
                if (afterSessions.Count() > 0)
                    dates = "<h4 class='text-success'>The Following Dates Are Available :</h4>";
                foreach (Session session in afterSessions)
                {
                    dates += "<li>" + session.Time.ToString("yyyy-MM-dd") + "</li>";
                }
                return Content("<h4 class='text-danger'>No Session Available For The Selected Date</h4><ul>" + dates + "</ul>", "text/html");
            }
            return PartialView("SessionSelectionPartial", new MovieSessionsViewModel() { Sessions = sessions });
        }


        public ActionResult RetrieveSeats(int id)
        {
            var session = this.dataBase.Sessions.Include(s=>s.Cinema).Include(s=>s.Seats).Where(s=>s.SessionID == id).Single();
            return PartialView("SeatSelectionPartial", new SeatsSelectionViewModel() { Seats = session.Seats, Cinema = session.Cinema });
        }


        private bool ConfirmSeats(int SessionID, int[] SelectedSeats)
        {
            foreach (int seatNum in SelectedSeats)
            {
                var seat = this.dataBase.Seats.Where(s => s.SessionID == SessionID && s.SeatNumber == seatNum).Single();
                if (seat.Availability == SeatAvailability.FREE)
                {
                    seat.Availability = SeatAvailability.TAKEN;
                }
                else {
                    return false;
                }

            }
            this.dataBase.SaveChanges();
            return true;
        }

        public ActionResult BookingSucceed()
        {
            return View("BookingSuccess");
        }

        [HttpPost]
        public async Task<ActionResult> AcceptPayment(BookAMovieViewModel vm)
        {
            if (vm.Token == null)
                ModelState.AddModelError("", vm.ErrorMessage);

            if (ModelState.IsValid && !ConfirmSeats(vm.SessionID, vm.SelectedSeats))
                ModelState.AddModelError("", "At Least One Selected Seat Is Taken , Please Reselect");

            if (ModelState.IsValid)
            {
                try
                {
                    var chargeId = await ProcessPayment(vm);
                    var transaction = new Transaction() { Amount = vm.Total, SessionID = vm.SessionID, Date = DateTime.Now };
                    if (Request.IsAuthenticated)
                    {
                        var user = this.dataBase.Users.Find(User.Identity.GetUserId());
                        transaction.UserID = user.Id;
                    }
                    this.dataBase.Transactions.Add(transaction);
                    this.dataBase.SaveChanges();
                }
                catch (StripeException e)
                {
                    return Content(e.Message);
                }
                return new EmptyResult();
            }
            else
            {
                var message = "<ul>";
                foreach (ModelState modelState in ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        message += "<li class='text-danger'>" + error.ErrorMessage + "</li>";
                    }
                }
                message += "</ul>";
                return PartialView("WarningModal", ViewBag.Content = message);
            }
        }

        private static async Task<string> ProcessPayment(BookAMovieViewModel vm)
        {
            return await Task.Run(() =>
            {
                var myCharge = new StripeChargeCreateOptions
                {
                    Amount = (int)(vm.Total * 100),
                    Currency = "aud",
                    Description = vm.Token + "@" + vm.HolderName,
                    Source = new StripeSourceOptions
                    {
                        TokenId = vm.Token
                    }
                };

                var chargeService = new StripeChargeService("sk_test_WhqgOD3TcxtNwJWrbiOOoWWw");
                var stripeCharge = chargeService.Create(myCharge);
                return stripeCharge.Id;
            });
        }




        public ActionResult CreateConfirmationLetter()
        {
            using (var ms = new MemoryStream())
            {
                Document pdfDoc = new Document(PageSize.A4);
                PdfWriter.GetInstance(pdfDoc, ms);
                pdfDoc.Open();
                pdfDoc.Add(new Paragraph("Welcome to dotnetfox"));
                pdfDoc.Add(new Header("Header", "It is a header"));
                pdfDoc.Close();
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Disposition", "inline;test.pdf");
                Response.Buffer = true;
                Response.Clear();
                Response.OutputStream.Write(ms.GetBuffer(), 0, ms.GetBuffer().Length);
                Response.OutputStream.Flush();
                Response.End();
            }
            return new FileStreamResult(Response.OutputStream, "application/pdf");
        }

    }
}