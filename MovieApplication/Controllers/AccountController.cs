﻿using System;
using System.Web;
using System.Web.Mvc;
using MovieApplication.Models;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.Owin;
using MovieApplication.ViewModels;
using System.Net;
using Microsoft.Owin.Security;
using Microsoft.AspNet.Identity;

namespace MovieApplication.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private MovieApplicationContext dataBase;
        private MovieApplicationUserManager userManager;
        private MovieApplicationSignInManager signInManager;
        private MovieApplicationRoleManager roleManager;
        public MovieApplicationUserManager UserManager
        {
            get
            {
                return userManager ?? HttpContext.GetOwinContext().GetUserManager<MovieApplicationUserManager>();
            }
            private set
            {
                userManager = value;
            }
        }



        public MovieApplicationSignInManager SignInManager
        {

            private set
            {
                signInManager = value;
            }

            get
            {

                return signInManager ?? HttpContext.GetOwinContext().Get<MovieApplicationSignInManager>();
            }

        }
        public MovieApplicationRoleManager RoleManager
        {

            private set
            {
                roleManager = value;
            }

            get
            {

                return roleManager ?? HttpContext.GetOwinContext().Get<MovieApplicationRoleManager>();
            }

        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }



        public AccountController() : base()
        {
            this.dataBase = new MovieApplicationContext();
        }


        public AccountController(MovieApplicationUserManager userManager, MovieApplicationSignInManager signInManager) : this()
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> SignIn(SignInViewModel signInVM)
        {
            bool ifSucceed = true;
            if (ModelState.IsValid)
            {
                try
                {
                    var result = await SignInManager.PasswordSignInAsync(signInVM.Email, signInVM.Password, signInVM.IfRememberMe, false);
                    switch (result)
                    {
                        case SignInStatus.Success:
                            ifSucceed = true;
                            break;
                        default:
                            ModelState.AddModelError("", "Password Is Incorrect");
                            ifSucceed = false;
                            break;
                    }
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message);

                }

            }
            else
            {
                ifSucceed = false;
            }


            if (ifSucceed)
            {
                if (TempData["returnURL"] != null)
                    return Redirect((string)TempData["returnURL"]);
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return View("~/Views/Home/SignIn.cshtml", signInVM);
            }

        }





        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> SignUp(SignUpViewModel signUpVM)
        {
            bool ifSucceed = true;

            if (ModelState.IsValid)
            {
                User newUser = new User()
                {
                    Email = signUpVM.Email,
                    LastName = signUpVM.LastName,
                    FirstName = signUpVM.FirstName,
                    PhoneNumber = signUpVM.PhoneNumber,
                    UserName = signUpVM.Email,

                };
                try
                {
                    var result = await UserManager.CreateAsync(newUser, signUpVM.Password);

                    if (result.Succeeded)
                    {
                        if (newUser.Email == "admin@dcmovie.com") // set up an Admin Account
                            UserManager.AddToRole(newUser.Id, "Admin");
                        else
                            UserManager.AddToRole(newUser.Id, "User");
                        await SignInManager.SignInAsync(newUser, false, false);
                    }
                    else
                    {
                        foreach (var error in result.Errors)
                        {
                            if (!error.StartsWith("Name")) // eliminate this default userName error message  
                                ModelState.AddModelError("", error);
                        }
                        ifSucceed = false;
                    }

                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message);
                }

            }

            else
            {
                ifSucceed = false;
            }
            if (ifSucceed)
            {


                return RedirectToAction("SignUpSucceed");
            }
            else
            {

                return View("~/Views/Home/SignUp.cshtml", signUpVM);
            }



        }

        public ActionResult SignOut()
        {

            SignInManager.AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");

        }

        public ActionResult SignUpSucceed()
        {
            return View("SignUpSuccess");
        }

        public ActionResult ViewProfile()
        {
            var userId = User.Identity.GetUserId();
            var user = this.dataBase.Users.Find(userId);
            return View("Profile", new ProfileViewModel() { User = user });
        }

        public ActionResult EditProfile(ProfileViewModel vm)
        {
            var userId = User.Identity.GetUserId();
            var user = this.dataBase.Users.Find(userId);
            var result = new IdentityResult();
            if (vm.CurrentPassword != null && vm.Password != null)
                result = UserManager.ChangePassword(userId, vm.CurrentPassword, vm.Password);
            if (result.Errors != null)
            {
                foreach (string error in result.Errors)
                {
                    ModelState.AddModelError("", error);
                }
            }
            if (ModelState.IsValid)
            {
                user.PhoneNumber = vm.PhoneNumber;
                user.LastName = vm.LastName;
                user.FirstName = vm.FirstName;
                this.dataBase.SaveChanges();

            }
            else
            {
                vm.User = user;
                return View("Profile", vm);
            }
            return RedirectToAction("ViewProfile");
        }

    }
}