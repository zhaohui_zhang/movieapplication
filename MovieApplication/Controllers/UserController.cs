﻿using Microsoft.AspNet.Identity;
using MovieApplication.Models;
using MovieApplication.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace MovieApplication.Controllers
{
    [Authorize(Roles = "User")]
    public class UserController : Controller
    {
        private MovieApplicationContext database;

        public UserController()
        {
            this.database = new MovieApplicationContext();
        }

        public ActionResult Index()
        {
            var userId = User.Identity.GetUserId();
            var user = this.database.Users.Find(userId);
            var name = user.FirstName + " " + user.LastName;
            var top5Movies = this.database.Movies.Take(5).ToList().OrderByDescending(m => m.AverageRating).ToList();
            var transactions = this.database.Transactions.Include(t => t.Session.Movie).Where(t => t.UserID == userId).ToList();
            return View("DashBoard", new UserDashboardViewModel() { UserName = name, Transactions = transactions, Movies = top5Movies });
        }

    }
}