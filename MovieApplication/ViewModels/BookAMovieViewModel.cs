﻿using MovieApplication.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MovieApplication.ViewModels
{
    public class BookAMovieViewModel
    {
        public Movie Movie { get; set; }

        [Required]
        [Range(10000000000000, 9999999999999999, ErrorMessage = "Please Present A Valid Credit Card Number")]
        public long CardNumber { get; set; }

        [Required]
        public int ExpiryMonth { get; set; }

        [Required]
        public int ExpiryYear { get; set; }

        [Required]
        public string HolderName { get; set; }

        [Required]
        [Range(100, 999, ErrorMessage = "Please Enter A Valid CCV")]
        public int CCV { get; set; }


        public int[] SelectedSeats { get; set; }

        public int SessionID { get; set; }

        public string Token { get; set; }

        public string ErrorMessage { get; set; }

        public int Total { get; set; }
    }


}