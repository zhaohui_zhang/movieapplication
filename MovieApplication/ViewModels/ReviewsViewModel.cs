﻿using MovieApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MovieApplication.ViewModels
{
    public class ReviewsViewModel
    {
        public virtual ICollection<Review> Reviews { get; set; }


        public string Message {
            get {
                if (Reviews.Count == 0)
                    return "No Comment Yet";
                return string.Empty;
            }
        }

    }

   
}