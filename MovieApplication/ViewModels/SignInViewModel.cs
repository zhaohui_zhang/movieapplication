﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MovieApplication.ViewModels
{
    public class SignInViewModel : CredentialViewModel
    {
        private string rememberMe;
        public bool IfRememberMe { get; set; }
        public string RememberMe
        {
            set
            {
                this.rememberMe = value;
                switch (value)
                {
                    case "On":
                        IfRememberMe = true;
                        break;
                    default:
                        IfRememberMe = false;
                        break;
                }
            }
            get
            {
                return this.rememberMe;
            }
        }

        [Required(ErrorMessage = "Please Enter A Password Of Length Of At Least 6 Characters")]
        public new string Password { get; set; }

    }
}