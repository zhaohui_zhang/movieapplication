﻿using MovieApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MovieApplication.ViewModels
{
    public class TransactionsViewModel
    {
        public virtual ICollection<Transaction> Transactions { get; set; }
    }
}