﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MovieApplication.ViewModels
{
    public class CredentialViewModel
    {
        [Required(ErrorMessage = "Email Cannot Be Empty")]
        [RegularExpression(@"^.+@.+\.com$", ErrorMessage = "Please Enter A Valid Email Address")]
        public string Email {get;set;}

        public string Password { get; set; }

 

    }
}