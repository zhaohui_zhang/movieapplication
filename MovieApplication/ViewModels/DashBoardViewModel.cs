﻿using MovieApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MovieApplication.ViewModels
{
    public class DashBoardViewModel
    {
        public  ICollection<Movie> MoviesByRating { get; set; }

        public  ICollection<Movie> MovieByRevenue { get; set; }

        public  ICollection<Session> Sessions { get; set; }

        public  ICollection<Slide> Slides { get; set; }
    }
}