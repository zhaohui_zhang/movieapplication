﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MovieApplication.ViewModels
{
    public class SignUpViewModel : AccountBaseViewModel
    {

        [Required(ErrorMessage = "Please Enter A Password Of Length Of At Least 6 Characters")]
        public new string Password { get; set; }

    }
}