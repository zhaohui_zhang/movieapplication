﻿using MovieApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MovieApplication.ViewModels
{
    public class UserDashboardViewModel
    {
        public string UserName { get; set; }

        public ICollection<Transaction> Transactions {get ; set; }

        public ICollection<Movie> Movies { get; set; }

     }
}