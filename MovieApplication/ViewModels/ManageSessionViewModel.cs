﻿using MovieApplication.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MovieApplication.ViewModels
{
    public class ManageSessionViewModel : MoviesViewModel
    {
        public ICollection<Cinema> Cinemas { get; set; }

        [Required(ErrorMessage = "Please Provide A Ticket Price")]
        [Range(1,int.MaxValue , ErrorMessage = "Please Provide A Valid Price")]
        public int Price { get; set; }

        public string Date { get; set; }

        public int Hour { get; set; }

        public int Minute { get; set; }

        public int CinemaID { get; set; }

        public int MovieID { get; set; }
    }
}