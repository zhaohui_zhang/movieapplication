﻿using MovieApplication.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MovieApplication.ViewModels
{
    public class ProfileViewModel : AccountBaseViewModel
    {
        public User User { get; set; }

        [MinLength(6, ErrorMessage = "Please Enter A Password Of At Least 6 Characters")]
        public string CurrentPassword { get; set; }

        [MinLength(6, ErrorMessage = "Please Enter A Password Of At Least 6 Characters")]
        public new string Password { get; set; }

    }
}