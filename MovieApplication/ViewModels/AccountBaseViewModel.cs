﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MovieApplication.ViewModels
{
    public class AccountBaseViewModel : CredentialViewModel
    {
        [Compare("Password", ErrorMessage = "Please Enter The Same Password")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "First Name Cannot Be Empty")]
        [DataType(DataType.Text, ErrorMessage = "Please Enter A Valid First Name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Last Name Cannot Be Empty")]
        [DataType(DataType.Text, ErrorMessage = "Please Enter A Valid Last Name")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Phone Number Cannot Be Empty")]
        public string PhoneNumber { get; set; }


    }
}