﻿using MovieApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MovieApplication.ViewModels
{
    public class IndexViewModel
    {
        public virtual ICollection<Slide> Slides { get; set; }
    }
}