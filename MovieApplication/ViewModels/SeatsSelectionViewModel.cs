﻿using MovieApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MovieApplication.ViewModels
{
    public class SeatsSelectionViewModel
    {
        public ICollection<Seat> Seats { get; set; }

        public Cinema Cinema { get; set; }
    }
}